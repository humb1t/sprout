# Sprout Assignment

## Simple Web application for substitution expressions

### Run

```bash
cargo run
```

### Test

#### Base rules

```bash
curl --request POST \
  --url http://127.0.0.1:8080/base \
  --header 'content-type: application/json' \
  --data '{
	"a": true,
	"b": true,
	"c": true,
	"d": 10.0,
	"e": 10,
	"f": 0
}'
```

#### Custom1 rules

```bash
curl --request POST \
  --url http://127.0.0.1:8080/custom1 \
  --header 'content-type: application/json' \
  --data '{
	"a": true,
	"b": true,
	"c": true,
	"d": 10.0,
	"e": 10,
	"f": 0
}'
```

#### Custom2 rules

```bash
curl --request POST \
  --url http://127.0.0.1:8080/custom2 \
  --header 'content-type: application/json' \
  --data '{
	"a": true,
	"b": true,
	"c": true,
	"d": 10.0,
	"e": 10,
	"f": 0
}'
```

## Solution description

Solution is quite simple. Project library part consist of Input and Output structures for Inputs and Outputs accordingly and Rules enumeration for substitution.

Execution logic is build on `if` conditions. It was decided just to copy `Base` branch into `Custom1` and `Custom2` with small changes instead of writing additional set of abstractions to make solution "simple and stupid" having only small chunk of initial requirements.

`lib` part covered with "non-IO" tests and did not contain Web related functionality. Web server and routing implemented inside `main` part of the application.
