use tide::{Request, Body};
use sprout::*;

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    tide::log::start();
    let mut app = tide::new();
    app.at("/base").post(|mut req: Request<()>| async move {
        let input: Input = req.body_json().await?;
        let output = Rules::Base.apply(&input).expect("Failed to apply Rule.");
        Ok(Body::from_json(&output)?)
    });
    app.at("/custom1").post(|mut req: Request<()>| async move {
        let input: Input = req.body_json().await?;
        let output = Rules::Custom1.apply(&input).expect("Failed to apply Rule.");
        Ok(Body::from_json(&output)?)
    });
    app.at("/custom2").post(|mut req: Request<()>| async move {
        let input: Input = req.body_json().await?;
        let output = Rules::Custom2.apply(&input).expect("Failed to apply Rule.");
        Ok(Body::from_json(&output)?)
    });
    app.listen("127.0.0.1:8080").await?;
    Ok(())
}
