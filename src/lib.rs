use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct Input {
    a: bool,
    b: bool,
    c: bool,
    d: f32,
    e: i32,
    f: i32,
}

#[derive(Debug, Serialize)]
pub struct Output {
    h: Mpt,
    k: f32,
}

#[derive(Debug, PartialEq, Serialize)]
pub enum Mpt {
    M,
    P,
    T,
}

#[derive(Debug)]
pub enum Rules {
    Base,
    Custom1,
    Custom2,
}

impl Rules {
    pub fn apply(&self, input: &Input) -> Result<Output, &'static str> {
        match self {
            Rules::Base => {
                if input.a {
                    if input.c {
                        Ok(Output {
                            h: Mpt::P,
                            k: input.d + (input.d * (input.e as f32 - input.f as f32) / 25.5),
                        })
                    } else if input.b {
                        Ok(Output {
                            h: Mpt::M,
                            k: input.d + (input.d * input.e as f32 / 10.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                } else {
                    if input.b && input.c {
                        Ok(Output {
                            h: Mpt::T,
                            k: input.d - (input.d * input.f as f32 / 30.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                }
            }
            Rules::Custom1 => {
                if input.a {
                    if input.b && input.c {
                        Ok(Output {
                            h: Mpt::P,
                            k: 2.0 * input.d + (input.d * input.e as f32 / 100.0),
                        })
                    } else if input.b {
                        Ok(Output {
                            h: Mpt::M,
                            k: input.d + (input.d * input.e as f32 / 10.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                } else {
                    if input.b && input.c {
                        Ok(Output {
                            h: Mpt::T,
                            k: input.d - (input.d * input.f as f32 / 30.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                }
            }
            Rules::Custom2 => {
                if input.a {
                    if !input.b && input.c {
                        Ok(Output {
                            h: Mpt::M,
                            k: input.f as f32 + input.d + (input.d * input.e as f32 / 100.0),
                        })
                    } else if input.b && input.c {
                        Ok(Output {
                            h: Mpt::P,
                            k: 2.0 * input.d + (input.d * input.e as f32 / 100.0),
                        })
                    } else if input.b && !input.c {
                        Ok(Output {
                            h: Mpt::T,
                            k: input.d + (input.d * input.e as f32 / 10.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                } else {
                    if input.b && input.c {
                        Ok(Output {
                            h: Mpt::T,
                            k: input.d - (input.d * input.f as f32 / 30.0),
                        })
                    } else {
                        Err("No branch available for input.")
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn base_abnotc() {
        let result = Rules::Base
            .apply(&Input {
                a: true,
                b: true,
                c: false,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::M);
        assert_eq!(result.k, 20.0);
    }

    #[test]
    fn base_abc() {
        let result = Rules::Base
            .apply(&Input {
                a: true,
                b: true,
                c: true,
                d: 10.0,
                e: 51,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::P);
        assert_eq!(result.k, 30.0);
    }

    #[test]
    fn base_notabc() {
        let result = Rules::Base
            .apply(&Input {
                a: false,
                b: true,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::T);
        assert_eq!(result.k, 10.0);
    }

    #[test]
    fn custom1_abnotc() {
        let result = Rules::Custom1
            .apply(&Input {
                a: true,
                b: true,
                c: false,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::M);
        assert_eq!(result.k, 20.0);
    }

    #[test]
    fn custom1_abc() {
        let result = Rules::Custom1
            .apply(&Input {
                a: true,
                b: true,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::P);
        assert_eq!(result.k, 21.0);
    }

    #[test]
    fn custom1_notabc() {
        let result = Rules::Custom1
            .apply(&Input {
                a: false,
                b: true,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::T);
        assert_eq!(result.k, 10.0);
    }

    #[test]
    fn custom2_abnotc() {
        let result = Rules::Custom2
            .apply(&Input {
                a: true,
                b: true,
                c: false,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::T);
        assert_eq!(result.k, 20.0);
    }

    #[test]
    fn custom2_anotbc() {
        let result = Rules::Custom2
            .apply(&Input {
                a: true,
                b: false,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::M);
        assert_eq!(result.k, 11.0);
    }

    #[test]
    fn custom2_abc() {
        let result = Rules::Custom2
            .apply(&Input {
                a: true,
                b: true,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::P);
        assert_eq!(result.k, 21.0);
    }

    #[test]
    fn custom2_notabc() {
        let result = Rules::Custom2
            .apply(&Input {
                a: false,
                b: true,
                c: true,
                d: 10.0,
                e: 10,
                f: 0,
            })
            .expect("Failed to apply Rule.");
        assert_eq!(result.h, Mpt::T);
        assert_eq!(result.k, 10.0);
    }
}
